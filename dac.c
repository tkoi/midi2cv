#include "dac.h"

#define DAC_SCK     LATA4
#define DAC_SDI     LATA2
#define DAC_CS0     LATA3
#define DAC_CS1     LATA1

void DAC_Init(void)
{
    DAC_SCK = 0;
    DAC_CS0 = 1;
    DAC_CS1 = 1;
}

void DAC_Set(dac_t dac, uint16_t code)
{
    if (dac == DAC_NONE)
    {
        return;
    }

    uint8_t cs0 = (dac & 0x2) >> 1;
    
    // In case this is unit 2
    dac &= 0x3;
    
    // Select which DAC on the chip
    code |= dac & 0x1 ? 0x9000 : 0x1000;
    
    // Disable interrupts
    di();
    
    // Select which chip
    DAC_CS0 = cs0;
    DAC_CS1 = !cs0;
    
    // Bitbanged SPI
    // A loop is prettier but it's way faster unrolled
#define BITBANG code <<= 1; DAC_SCK = 0; DAC_SDI = STATUSbits.CARRY; DAC_SCK = 1;
    BITBANG // 1
    BITBANG // 2
    BITBANG // 3
    BITBANG // 4
    BITBANG // 5
    BITBANG // 6
    BITBANG // 7
    BITBANG // 8
    BITBANG // 9
    BITBANG // 10
    BITBANG // 11
    BITBANG // 12
    BITBANG // 13
    BITBANG // 14
    BITBANG // 15
    BITBANG // 16
    
    asm("nop"); // stretch the last clock pulse a little bit
    DAC_SCK = 0;
    
    // Deselect both chips
    DAC_CS0 = 1;
    DAC_CS1 = 1;
    
    // Enable interrupts
    ei();
}

