import optparse

# HEX_FILE = "main.hex"
# MIDI_FILE = "midi2cv.mid"
# LOG_FILE = "hex2midi.log"
SYSEX_BEGIN = 0xF0
SYSEX_END = 0xF7
SYSEX_HEADER_WRITEROW = bytes([0x7D, 0x25, 0x5E, 0x01])
SYSEX_RESET_COMMAND = bytes([SYSEX_BEGIN, 6, 0x7D, 0x25, 0x5E, 0x00, 0x19, SYSEX_END])

################################################################################

parser = optparse.OptionParser()
parser.add_option(
    '-i',
    '--input',
    dest='input_file',
    default=None,
    help='Read input from FILE',
    metavar='FILE')
parser.add_option(
    '-o',
    '--output',
    dest='output_file',
    default=None,
    help='Write output to FILE',
    metavar='FILE')
parser.add_option(
    '-l',
    '--log',
    dest='log_file',
    default=None,
    help='Write logging data to FILE',
    metavar='FILE')

options, args = parser.parse_args()

################################################################################

class MidiFirmware():
    def __init__(self, filePath, logFilePath):
        self.filePath = filePath
        self.midiData = bytearray()
        self.logFilePath = logFilePath
        if self.logFilePath != None:
            with open(self.logFilePath, 'w') as log:
                log.write("")

    def add_row(self, address, data):
        try:
            assert(address > 0x1FF and address < 0x4000)
            assert((address & 0x1F) == 0)
            assert(len(data) == 64)
            self.midiData.append(30) # 30ms delta time since last event
            self.midiData.append(SYSEX_BEGIN)
            self.midiData.append(72) # length field
            self.midiData += SYSEX_HEADER_WRITEROW
            self.midiData.append(address >> 7)
            self.midiData.append(address & 0x7F)
            self.midiData += data
            self.checksum = 0x7F & -(SYSEX_BEGIN + SYSEX_END + sum(SYSEX_HEADER_WRITEROW + data) + (address >> 7) + (address & 0x7F))
            self.midiData += bytes([self.checksum, SYSEX_END])

            if self.logFilePath != None:
                with open(self.logFilePath, 'a') as log:
                    print("[%04x] %s" % (address, data.hex()), file = log)
        except:
            print("[%02x @ %04x] %s" % (len(data), address, data.hex()))
            raise

    def write_out(self):
        with open(self.filePath, 'wb') as midi:
            midi.write(b'MThd\0\0\0\6\0\0\0\1')
            midi.write(bytes([0x01, 0xF4])) # 500 ticks/qn = 1 tick/ms
            midi.write(b'MTrk')
            self.midiData += bytes([30]) + SYSEX_RESET_COMMAND
            self.midiData += bytes([30, 0xFF, 0x2F, 0x00]) # end of track
            midi.write(len(self.midiData).to_bytes(4, "big"))
            midi.write(self.midiData)

################################################################################

extendedAddress = 0
rowData = bytearray()
rowAddress = 0
byteAddress = 0
midi = MidiFirmware(options.output_file, options.log_file)

with open(options.input_file, 'r') as hex:
    for line in hex:
        assert(line[0] == ':')
        linebytes = bytes.fromhex(line.strip(':\r\n'))

        byteCount = linebytes[0]
        lineAddress = (linebytes[1] << 8) | linebytes[2]
        recType = linebytes[3]
        data = linebytes[4:len(linebytes)-1]
        checksum = linebytes[len(linebytes)-1]
        bytesSum = sum(linebytes)

        assert((bytesSum & 0xFF) == 0)

        if recType == 1: # end of file
            break;

        if recType == 4: # extended address
            extendedAddress = (data[0] << 8) | data[1]

        if recType == 0: # data
            lineAddress += (extendedAddress << 16)
            if lineAddress > 0xFFFF:
                continue

            assert(len(data) & 1 == 0)
            assert(len(rowData) & 1 == 0)

            while lineAddress > byteAddress:
                # fill in skipped addresses
                if lineAddress < rowAddress + 0x40:
                    rowData += bytes([0x7F] * (lineAddress - byteAddress))
                    byteAddress = lineAddress
                elif len(rowData) > 0:
                    rowData += bytes([0x7F] * (0x40 - len(rowData)))
                    midi.add_row(rowAddress >> 1, rowData)
                    rowData.clear()
                    rowAddress = lineAddress & 0xFFC0
                    byteAddress = rowAddress
                else:
                    rowAddress = lineAddress & 0xFFC0
                    byteAddress = rowAddress

            for i in range(0, len(data), 2):
                word = data[i] | (data[i+1] << 8)
                assert((word & 0xC000) == 0)
                rowData.append(word >> 7)
                rowData.append(word & 0x7F)
                byteAddress += 2
                if len(rowData) == 64:
                    midi.add_row(rowAddress >> 1, rowData)
                    rowAddress += 0x40
                    rowData.clear()

################################################################################

midi.write_out()
