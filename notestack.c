// The monophonic voice allocation logic in this file is based on note_stack.h 
// from the Mutable Instruments CVpal firmware, designed by Olivier Gillet.
// http://mutable-instruments.net/modules/cvpal
// https://github.com/pichenettes/cvpal/tree/master/cvpal
//
// -----------------------------------------------------------------------------
//
// Copyright 2011 Olivier Gillet.
//
// Author: Olivier Gillet (ol.gillet@gmail.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -----------------------------------------------------------------------------
//
// Stack of currently pressed keys.
//
// Currently pressed keys are stored as a linked list. The linked list is used
// as a LIFO stack to allow monosynth-like behaviour. An example of such
// behaviour is:
// player presses and holds C4-> C4 is played.
// player presses and holds C5 (while holding C4) -> C5 is played.
// player presses and holds G4 (while holding C4&C5)-> G4 is played.
// player releases C5 -> G4 is played.
// player releases G4 -> C4 is played.
//
// The nodes used in the linked list are pre-allocated from a pool of 16
// nodes, so the "pointers" (to the root element for example) are not actual
// pointers, but indices of an element in the pool.
//
// Additionally, an array of pointers is stored to allow random access to the
// n-th note, sorted by ascending order of pitch (for arpeggiation).

#include "notestack.h"

void NoteStack_NoteOn(NoteStack_t *ns, uint8_t note, uint8_t velocity)
{
    // Remove the note from the list first (in case it is already here).
    NoteStack_NoteOff(ns, note);
    
    // In case of saturation, remove the least recently played note from the stack.
    if (ns->pool_size == ns->capacity)
    {
        uint8_t least_recent_note;
        for (uint8_t i = 1; i <= ns->capacity; ++i)
        {
            if (ns->pool[i].next_index == NULL)
            {
                least_recent_note = ns->pool[i].note;
            }
        }
        NoteStack_NoteOff(ns, least_recent_note);
    }
    
    // Now we are ready to insert the new note. Find a free slot to insert it.
    uint8_t free_slot;
    for (uint8_t i = 1; i <= ns->capacity; ++i)
    {
        if (ns->pool[i].note == FREE_SLOT)
        {
            free_slot = i;
            break;
        }
    }
    ns->pool[free_slot].next_index = ns->top_index;
    ns->pool[free_slot].note = note;
    ns->pool[free_slot].velocity = velocity;
    ns->top_index = free_slot;
    ns->pool_size++;
}

void NoteStack_NoteOff(NoteStack_t *ns, uint8_t note)
{
    uint8_t current = ns->top_index;
    uint8_t previous = NULL;
    
    while (current)
    {
        if (ns->pool[current].note == note)
        {
            break;
        }
        previous = current;
        current = ns->pool[current].next_index;
    }
    
    if (current)
    {
        if (previous)
            ns->pool[previous].next_index = ns->pool[current].next_index;
        else
            ns->top_index = ns->pool[current].next_index;
        
        ns->pool[current].note = FREE_SLOT;
        ns->pool_size--;
    }
}

void NoteStack_Clear(NoteStack_t *ns)
{
    ns->pool_size = 0;
    ns->top_index = NULL;
    
    // Initialize dummy node to known values
    // The dummy node is used to hold CVs after all keys are released
    ns->pool[0].note = 24;
    ns->pool[0].velocity = 64;
    
    for (uint8_t i = 1; i <= ns->capacity; ++i)
    {
        ns->pool[i].note = FREE_SLOT;
    }
}

void NoteStack_Init(NoteStack_t *ns, uint8_t capacity)
{
    ns->capacity = capacity;

    NoteStack_Clear(ns);
}

