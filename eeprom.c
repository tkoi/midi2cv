#include "eeprom.h"

#define BLOCK_NONE                  -1

// Need a checksum that will reject all-zero and all-one blocks
#define EXPECTED_CHECKSUM           0x71

// Block field offsets
#define BLOCK_OFFSET_CHECKSUM       0
#define BLOCK_OFFSET_SEQUENCE_NUM   1
#define BLOCK_OFFSET_DATA           2

typedef struct eeprom_page
{
    uint8_t start_address;
    uint8_t block_size;
    uint8_t num_blocks;
    int16_t current_block_address;
} page_t;

static page_t g_SettingsPage, g_CalibrationPage;
static settings_t g_Settings;
static calibration_t g_Calibration;

static bool_t InitPage(page_t *pPage, uint8_t startAddress, uint8_t blockSize, uint8_t numBlocks);
static bool_t BlockIsValid(page_t *pPage, uint8_t address);
static bool_t LoadBlock(page_t *pPage, uint8_t *buffer, uint8_t length);
static bool_t WriteBlock(page_t *pPage, uint8_t *buffer, uint8_t length);

//==============================================================================

void EEPROM_Init(void)
{
    // Initialize EEPROM pages.
    InitPage(&g_SettingsPage, 0, 16, 8);
    InitPage(&g_CalibrationPage, 128, 42, 3);
    
    // Load settings from EEPROM
    uint8_t *buffer = (uint8_t *)&g_Settings;
    bool_t success = LoadBlock(&g_SettingsPage, buffer, sizeof(g_Settings));
    
    if (success)
    {
        // sanitize non-bool settings

        switch (g_Settings.cv2_mode)
        {
        case SETTINGS_CV2_MODE_NONE:
        case SETTINGS_CV2_MODE_NOTE:
        case SETTINGS_CV2_MODE_CC:
        case SETTINGS_CV2_MODE_CHPRESSURE:
            break;
        default:
            g_Settings.cv2_mode = SETTINGS_CV2_MODE_NONE;
        }

        if (g_Settings.cv2_cc & 0x80)
        {
            g_Settings.cv2_cc = 0;
        }

        switch (g_Settings.divider)
        {
        case SETTINGS_DIVIDER_1:
        case SETTINGS_DIVIDER_2:
        case SETTINGS_DIVIDER_3:
        case SETTINGS_DIVIDER_4:
        case SETTINGS_DIVIDER_6:
        case SETTINGS_DIVIDER_8:
        case SETTINGS_DIVIDER_12:
        case SETTINGS_DIVIDER_24:
            break;
        default:
            g_Settings.divider = SETTINGS_DIVIDER_1;
        }

        switch (g_Settings.stack_mode)
        {
        case SETTINGS_STACK_MODE_NONE:
        case SETTINGS_STACK_MODE_UNIT1:
        case SETTINGS_STACK_MODE_UNIT2:
            break;
        default:
            g_Settings.stack_mode = SETTINGS_STACK_MODE_NONE;
        }

        if (g_Settings.num_voices > 4 || g_Settings.num_voices < 1)
        {
            g_Settings.num_voices = 4;
        }
    }
    else
    {
        // Set default values
        g_Settings.cv2_mode        = SETTINGS_CV2_MODE_NONE;
        g_Settings.cv2_cc          = 0;
        g_Settings.divider         = SETTINGS_DIVIDER_1;
        g_Settings.exp_velocity    = true;
        g_Settings.retrigger       = true;
        g_Settings.regate          = false;
        g_Settings.stack_mode      = SETTINGS_STACK_MODE_NONE;
        g_Settings.num_voices      = 4;
        g_Settings.velocity_unit   = false;
        g_Settings.persistent_lock = false;
    }
    
    buffer = (uint8_t *)&g_Calibration;
    success = LoadBlock(&g_CalibrationPage, buffer, sizeof(g_Calibration));
    
    if (success == false)
    {
        // Set default calibration
        for (uint8_t i = 0; i < CALIBRATE_NUM_CV_OUTS; i++)
        {
            for (uint8_t j = 0; j < CALIBRATE_LUT_SIZE; j++)
            {
                CALIBRATION(i, j) = 0;
            }
        }
    }
}

/** \brief Save settings to EEPROM.
 *  \return \c true if settings were successfully saved, \c false if not.
 */
bool_t EEPROM_SaveSettings(void)
{
    uint8_t *buffer = (uint8_t *)&g_Settings;
    
    return WriteBlock(&g_SettingsPage, buffer, sizeof(g_Settings));
}

inline settings_t *EEPROM_GetSettings(void)
{
    return &g_Settings;
}

/** \brief Save calibration data to EEPROM.
 *  \return \c true if calibration data was successfully saved, \c false if not.
 */
bool_t EEPROM_SaveCalibration(void)
{
    uint8_t *buffer = (uint8_t *)&g_Calibration;
    
    return WriteBlock(&g_CalibrationPage, buffer, sizeof(g_Calibration));
}

inline calibration_t *EEPROM_GetCalibration(void)
{
    return &g_Calibration;
}

//==============================================================================

/** \brief Initialize a page in EEPROM.
 *  \param pPage Pointer to a \c page_t specifying the page to initialize.
 *  \param startAddress Starting address of the page in EEPROM.
 *  \param blockSize The size of each block in the page. Must be at least 2 bytes larger than
 *         the number of bytes of data it will hold.
 *  \param numBlocks The number of blocks in the page.
 *  \return \c true if a valid block was found in the page, \c false if not.
 */
static bool_t InitPage(page_t *pPage, uint8_t startAddress, uint8_t blockSize, uint8_t numBlocks)
{
    int16_t prevBlockAddress = BLOCK_NONE;
    uint8_t lastBlockAddress = startAddress + blockSize*(numBlocks - 1);
    uint8_t sn1, sn2;
    
    pPage->start_address = startAddress;
    pPage->block_size = blockSize;
    pPage->num_blocks = numBlocks;
    
    for (uint8_t address = startAddress; address <= lastBlockAddress; address += blockSize)
    {
        if (BlockIsValid(pPage, address))
        {
            if (prevBlockAddress != BLOCK_NONE)
            {
                // Get the sequence numbers for the current and new parameter blocks.
                sn1 = eeprom_read(prevBlockAddress + BLOCK_OFFSET_SEQUENCE_NUM);
                sn2 = eeprom_read(address + BLOCK_OFFSET_SEQUENCE_NUM);

                // See if the sequence number for the new parameter block is
                // greater than the current block.  The comparison isn't
                // straightforward since the one byte sequence number will wrap
                // after 256 parameter blocks.
                if ( (sn1 > sn2 && (sn1 - sn2) < 128) || (sn2 > sn1 && (sn2 - sn1) > 128) )
                {
                    // The new parameter block is older than the current
                    // parameter block, so skip the new parameter block and
                    // keep searching.
                    continue;
                }
            }

            // The new parameter block is more recent than the current one, so
            // make it the new current parameter block.
            prevBlockAddress = address;
        }
    }

    // Save the address of the most recent parameter block found.  If no valid
    // parameter blocks were found, this will be equal to BLOCK_NONE.
    pPage->current_block_address = prevBlockAddress;
    
    return (prevBlockAddress != BLOCK_NONE);
}

/** \brief Check if a parameter block is valid.
 *  \param pPage Pointer to a \c page_t specifying the page containing the block.
 *  \param address The address of the block in EEPROM.
 *  \return \c true if the block is valid, \c false if not.
 */
static bool_t BlockIsValid(page_t *pPage, uint8_t address)
{
    uint8_t sum = 0;

    for (uint8_t i = 0; i < (pPage->block_size); i++)
    {
        sum += eeprom_read(address + i);
    }

    return (sum == EXPECTED_CHECKSUM);
}

/** \brief Load a parameter block from EEPROM.
 *  \param pPage Pointer to a \c page_t specifying the page containing the block.
 *  \param buffer Pointer to an array to load the block into.
 *  \param length Number of bytes to copy from EEPROM into \c buffer.
 *  \return \c true if the block was successfully loaded, \c false if not.
 */
static bool_t LoadBlock(page_t *pPage, uint8_t *buffer, uint8_t length)
{
    if (pPage->current_block_address != BLOCK_NONE)
    {
        for (uint8_t i = 0; i < length; i++)
        {
            buffer[i] = eeprom_read(pPage->current_block_address + BLOCK_OFFSET_DATA + i);
        }
        
        return true;
    }
    else
    {
        return false;
    }
}

/** \brief Write a parameter block to EEPROM.
 *  \param pPage Pointer to a \c page_t specifying the page containing the block.
 *  \param buffer Pointer to an array to write to the block.
 *  \param length Length of \c buffer.
 *  \return \c true if the block was successfully written, \c false if not.
 */
static bool_t WriteBlock(page_t *pPage, uint8_t *buffer, uint8_t length)
{
    uint8_t sn;
    uint8_t sum;
    uint16_t newBlockAddress;
    
    // See if there is a valid parameter block in EEPROM.
    if (pPage->current_block_address != BLOCK_NONE)
    {
        // Set the sequence number to one greater than the most recent parameter block.
        sn = eeprom_read(pPage->current_block_address + BLOCK_OFFSET_SEQUENCE_NUM) + 1;

        newBlockAddress = pPage->current_block_address + pPage->block_size;
        
        uint8_t lastBlockAddress = pPage->start_address + (pPage->block_size)*(pPage->num_blocks - 1);
        
        if (newBlockAddress > lastBlockAddress)
        {
            newBlockAddress = pPage->start_address;
        }
    }
    else
    {
        sn = 0;
        newBlockAddress = pPage->start_address;
    }
    
    // Compute the checksum.
    sum = EXPECTED_CHECKSUM - sn;
    for (uint8_t i = 0; i < length; i++)
    {
        sum -= buffer[i];
    }
    
    // Write the block. The block size may be larger than necessary, so
    // write zeros in the extra space so the checksum will be correct.
    eeprom_write(newBlockAddress + BLOCK_OFFSET_CHECKSUM, sum);
    eeprom_write(newBlockAddress + BLOCK_OFFSET_SEQUENCE_NUM, sn);
    for (uint8_t i = 0; i < (pPage->block_size - BLOCK_OFFSET_DATA); i++)
    {
        eeprom_write(newBlockAddress + BLOCK_OFFSET_DATA + i, (i < length) ? buffer[i] : 0x00);
    }
    
    // Check whether the block was correctly written.
    sum = 0;
    
    for (uint8_t i = 0; i < (pPage->block_size); i++)
    {
        sum += eeprom_read(newBlockAddress + i);
    }
    
    if (sum == EXPECTED_CHECKSUM)
    {
        // The new parameter block becomes the most recent parameter block.
        pPage->current_block_address = newBlockAddress;
        return true;
    }
    else
    {
        return false;
    }
}

