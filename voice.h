#ifndef VOICE_H_
#define VOICE_H_

#include "midi2cv.h"
#include "notestack.h"

typedef struct Voice
{
    NoteStack_t notestack;
    dac_t pitch_dac;
    dac_t velocity_dac;
    dac_t mod_dac;
    dac_t assignable_dac;
    pin_t gate;
    pin_t trig;
    uint8_t pitchbend; ///< Upper 7 bits of 14-bit pitchbend data
    uint8_t retrig_counter;
    uint8_t regate_counter;
    bool_t sustain;
} Voice_t;

void Voice_Init(Voice_t *voice, uint8_t stackSize, dac_t pitch, dac_t vel, dac_t mod, dac_t learn, pin_t gate, pin_t trig);
void Voice_NoteOn(Voice_t *v, uint8_t note, uint8_t velocity);
void Voice_NoteOff(Voice_t *v, uint8_t note, uint8_t velocity);
void Voice_Pitchbend(Voice_t *v, uint8_t lsbits, uint8_t msbits);
void Voice_ChannelPressure(Voice_t *v, uint8_t pressure);
void Voice_ControlChange(Voice_t *v, uint8_t control, uint8_t value);
inline void Voice_Tick(Voice_t *v);

#endif

