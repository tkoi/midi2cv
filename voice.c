#include "voice.h"
#include "eeprom.h"
#include "dac.h"

static const uint8_t g_ExpTable[128] =
{
   0,   1,   1,   1,   1,   1,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,
   3,   3,   3,   3,   3,   3,   3,   4,   4,   4,   4,   4,   4,   5,   5,   5,
   5,   5,   6,   6,   6,   6,   7,   7,   7,   7,   8,   8,   8,   8,   9,   9,
   9,  10,  10,  11,  11,  11,  12,  12,  13,  13,  13,  14,  14,  15,  15,  16,
  17,  17,  18,  18,  19,  20,  20,  21,  22,  22,  23,  24,  25,  25,  26,  27,
  28,  29,  30,  31,  32,  33,  34,  35,  37,  38,  39,  40,  42,  43,  44,  46,
  47,  49,  50,  52,  54,  56,  57,  59,  61,  63,  65,  67,  69,  72,  74,  76,
  79,  81,  84,  87,  90,  93,  96,  99, 102, 105, 108, 112, 116, 119, 123, 127
};

static void _Voice_Refresh(Voice_t *v)
{
    int16_t code;
    NoteStack_t *ns = &(v->notestack);
    
    if (v->pitch_dac != DAC_NONE)
    {
        code = Calibrate_GetCalibratedPitch(v->pitch_dac, ns->pool[ns->top_index].note);
        code = code + v->pitchbend - PITCHBEND_MIDPOINT;
        
        if (code < DAC_ZERO_SCALE)
        {
            code = DAC_ZERO_SCALE;
        }
        else if (code > DAC_FULL_SCALE)
        {
            code = DAC_FULL_SCALE;
        }
        
        DAC_Set(v->pitch_dac, code);
    }

    if (v->velocity_dac != DAC_NONE)
    {
        code = ns->pool[ns->top_index].velocity << 4; // approx 0-5V
        DAC_Set(v->velocity_dac, code);
    }
    
    if (v->assignable_dac != DAC_NONE && SETTINGS(cv2_mode) == SETTINGS_CV2_MODE_NOTE)
    {
        code = Calibrate_GetCalibratedPitch(v->assignable_dac, ns->pool[ns->top_index].note);
        code = code + v->pitchbend - PITCHBEND_MIDPOINT;
    
        if (code < DAC_ZERO_SCALE)
        {
            code = DAC_ZERO_SCALE;
        }
        else if (code > DAC_FULL_SCALE)
        {
            code = DAC_FULL_SCALE;
        }
        
        DAC_Set(v->assignable_dac, code);
    }
}

void Voice_Init(Voice_t *voice, uint8_t stackSize, dac_t pitch, dac_t vel, dac_t mod, dac_t learn, pin_t gate, pin_t trig)
{
    NoteStack_Init(&(voice->notestack), stackSize);

    voice->pitch_dac = pitch;
    voice->velocity_dac = vel;
    voice->mod_dac = mod;
    voice->assignable_dac = learn;
    voice->gate = gate;
    voice->trig = trig;

    voice->pitchbend = PITCHBEND_MIDPOINT;
    voice->retrig_counter = 0;
    voice->regate_counter = 0;
    voice->sustain = false;

    _Voice_Refresh(voice);
    MIDI2CV_PinClear(voice->gate);
    MIDI2CV_PinClear(voice->trig);
}

void Voice_NoteOn(Voice_t *v, uint8_t note, uint8_t velocity)
{
    NoteStack_t *ns = &(v->notestack);
    
    if (velocity == 0)
    {
        Voice_NoteOff(v, note, velocity);
    }
    else
    {
        if (SETTINGS(exp_velocity) == true)
        {
            velocity = g_ExpTable[velocity];
        }
        
        // Check this here so that a note on for a note that
        // is already on will cause a regate
        bool_t legato = (ns->pool_size > 0);
        
        NoteStack_NoteOn(ns, note, velocity);
        _Voice_Refresh(v);
        
        di();
        
        // Always trigger at first note on
        // Trigger at all note ons if retrigger mode is enabled
        if (legato == false || SETTINGS(retrigger) == true)
        {
            v->retrig_counter = PULSE_DURATION;
            MIDI2CV_PinSet(v->trig);
        }
        
        // Regate only at stacked note ons
        if (legato == true && SETTINGS(regate) == true)
        {
            v->regate_counter = PULSE_DURATION;
            MIDI2CV_PinClear(v->gate);
        }
        else
        {
            MIDI2CV_PinSet(v->gate);
        }
        
        ei();
    }
}

void Voice_NoteOff(Voice_t *v, uint8_t note, uint8_t velocity)
{
    NoteStack_t *ns = &(v->notestack);
    
    // Write to the dummy node so that when all keys are released, the
    // CVs will keep their previous values
    ns->pool[0].note = ns->pool[ns->top_index].note;
    ns->pool[0].velocity = ns->pool[ns->top_index].velocity;
    
    uint8_t top_note = ns->pool[ns->top_index].note;
    NoteStack_NoteOff(ns, note);
    
    // Retrigger logic lives here
    di();
    if (ns->pool_size > 0)
    {
        _Voice_Refresh(v);
        
        if (ns->pool[ns->top_index].note != top_note)
        {
            if (SETTINGS(retrigger) == true)
            {
                v->retrig_counter = PULSE_DURATION;
                MIDI2CV_PinSet(v->trig);
            }
            
            if (SETTINGS(regate) == true)
            {
                v->regate_counter = PULSE_DURATION;
                MIDI2CV_PinClear(v->gate);
            }
        }
    }
    else
    {
        if (v->sustain == false)
        {
            MIDI2CV_PinClear(v->gate);
            
            // Clear the regate counter so that the timer callback
            // doesn't reopen the gate.
            v->regate_counter = 0;
        }
    }
    ei();
}

void Voice_Pitchbend(Voice_t *v, uint8_t lsbits, uint8_t msbits)
{
    v->pitchbend = msbits;
    _Voice_Refresh(v);
}

void Voice_ControlChange(Voice_t *v, uint8_t control, uint8_t value)
{
    NoteStack_t *ns = &(v->notestack);
    uint16_t code;
    
    // First check if this is a channel mode message
    if (control >= 120)
    {
        if (control == MIDI_CC_ALL_SOUND_OFF && value == 0)
        {
            v->sustain = false;
            NoteStack_Clear(ns);
        }
        else if (control == MIDI_CC_RESET_ALL_CONTROLLERS && value == 0)
        {
            v->pitchbend = PITCHBEND_MIDPOINT;
            v->sustain = false;

            if (ns->pool_size == 0)
            {
                di();
                
                MIDI2CV_PinClear(v->gate);
                
                // Clear the regate counter so that the timer callback
                // doesn't reopen the gate.
                v->regate_counter = 0;
                
                ei();
            }
        }
        else if (control == MIDI_CC_ALL_NOTES_OFF && value == 0)
        {
            NoteStack_Clear(ns);
        }
        
        Voice_NoteOff(v, 0, 0); // This is kinda hacky but it will update the outputs
        return;
    }
    
    if (control == MIDI_CC_MOD)
    {
        code = value << 4; // approx 0-5V
        DAC_Set(v->mod_dac, code);
    }
    else if (control == MIDI_CC_SUSTAIN)
    {
        // On if value > 63
        if (value & 0x40)
        {
            v->sustain = true;
        }
        else
        {
            v->sustain = false;
            if (ns->pool_size == 0)
            {
                di();
                
                MIDI2CV_PinClear(v->gate);
                
                // Clear the regate counter so that the timer callback
                // doesn't reopen the gate.
                v->regate_counter = 0;
                
                ei();
            }
        }
    }
    
    if (SETTINGS(cv2_mode) == SETTINGS_CV2_MODE_CC && control == SETTINGS(cv2_cc))
    {
        code = value << 4; // approx 0-5V
        DAC_Set(v->assignable_dac, code);
    }
}

void Voice_ChannelPressure(Voice_t *v, uint8_t pressure)
{
    uint16_t code;
    
    if (SETTINGS(cv2_mode) == SETTINGS_CV2_MODE_CHPRESSURE)
    {
        code = pressure << 4;
        DAC_Set(v->assignable_dac, code);
    }
}

inline void Voice_Tick(Voice_t *v)
{
    // We don't need to disable interrupts for RMWs because
    // this function is only called from the interrupt.

    NoteStack_t *ns = &(v->notestack);
    
    if (v->retrig_counter > 0)
    {
        if (--(v->retrig_counter) == 0)
        {
            MIDI2CV_PinClear(v->trig);
        }
    }
    
    if (v->regate_counter > 0)
    {
        // Make sure at least one note is still held
        if (--(v->regate_counter) == 0 && ns->pool_size > 0)
        {
            MIDI2CV_PinSet(v->gate);
        }
    }
}