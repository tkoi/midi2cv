#include "midi2cv.h"

void MIDI2CV_PinSet(pin_t pin)
{
    switch (pin)
    {
    case 0x10: LATA0 = 1; break;
    case 0x11: LATA1 = 1; break;
    case 0x12: LATA2 = 1; break;
    case 0x13: LATA3 = 1; break;
    case 0x14: LATA4 = 1; break;
    case 0x16: LATA6 = 1; break;
    case 0x17: LATA7 = 1; break;
    case 0x20: LATB0 = 1; break;
    case 0x21: LATB1 = 1; break;
    case 0x22: LATB2 = 1; break;
    case 0x23: LATB3 = 1; break;
    case 0x24: LATB4 = 1; break;
    case 0x25: LATB5 = 1; break;
    case 0x26: LATB6 = 1; break;
    case 0x27: LATB7 = 1; break;
    }
}

void MIDI2CV_PinClear(pin_t pin)
{
    switch (pin)
    {
    case 0x10: LATA0 = 0; break;
    case 0x11: LATA1 = 0; break;
    case 0x12: LATA2 = 0; break;
    case 0x13: LATA3 = 0; break;
    case 0x14: LATA4 = 0; break;
    case 0x16: LATA6 = 0; break;
    case 0x17: LATA7 = 0; break;
    case 0x20: LATB0 = 0; break;
    case 0x21: LATB1 = 0; break;
    case 0x22: LATB2 = 0; break;
    case 0x23: LATB3 = 0; break;
    case 0x24: LATB4 = 0; break;
    case 0x25: LATB5 = 0; break;
    case 0x26: LATB6 = 0; break;
    case 0x27: LATB7 = 0; break;
    }
}
