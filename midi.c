#include "midi.h"
#include "seq.h"
#include "mono.h"
#include "poly1.h"
#include "poly4.h"
#include "eeprom.h"
#include "calibrate.h"
#include "led.h"

#define MIDI_STATUS_NOTE_OFF            0x8
#define MIDI_STATUS_NOTE_ON             0x9
#define MIDI_STATUS_AFTERTOUCH          0xA
#define MIDI_STATUS_CONTROL_CHANGE      0xB
#define MIDI_STATUS_PROGRAM_CHANGE      0xC
#define MIDI_STATUS_CHANNEL_PRESSURE    0xD
#define MIDI_STATUS_PITCHBEND           0xE
#define MIDI_STATUS_SYSTEM              0xF

#define MIDI_TIMING_CLOCK               0xF8
#define MIDI_START                      0xFA
#define MIDI_CONTINUE                   0xFB
#define MIDI_STOP                       0xFC
#define MIDI_RESET                      0xFF
#define MIDI_SYSEX_BEGIN                0xF0
#define MIDI_SYSEX_END                  0xF7

#define FUNCTION_KEY_CALIBRATE          20 // G#1

// We don't have a manufacturer ID so we'll just use the
// research/educational ID with two arbitrary ID bytes chosen
// by generating a random 14-bit number.
#define SYSEX_MFG_EDU                   0x7D
#define SYSEX_MIDI2CV_ID_1              0x25
#define SYSEX_MIDI2CV_ID_2              0x5E
#define SYSEX_MSG_ID_RESET              0
#define SYSEX_MSG_ID_LOCK_KEYS          1
#define SYSEX_MSG_ID_REPORT_VERSION     2

#define MAX_SYSEX_LENGTH                8 ///< Maximum length of SysEx messages we care about.
static uint8_t g_SysExBuffer[MAX_SYSEX_LENGTH];
static uint8_t g_SysExIndex;
static uint8_t g_SysExChecksum;

static struct
{
    uint8_t index;
    uint8_t length;
    union
    {
        struct
        {
            uint8_t status;
            uint8_t data1;
            uint8_t data2;
        };
        uint8_t byte[3];
    };
} g_Message;


static uint8_t g_Mode;
static uint8_t g_PrevMode; ///< For returning from calibration
static bool_t g_SystemExclusive;
static uint8_t g_RunningStatus;
static uint8_t g_LearnedChannel;
#define CHANNEL_NONE    0xFF

#define MAX_VOICES      4
static Voice_t g_Voice[MAX_VOICES];

// Indexed by lower 4 bits of status byte
static const uint8_t k_Length_of_system_common_message[8] =
{
    NULL,   // SysEx begin
    2,      // MIDI time code
    3,      // Song position pointer
    2,      // Song select
    1,      // Undefined
    1,      // Undefined
    1,      // Tune request
    1       // SysEx end
};

// Indexed by upper 4 bits of status byte
static const uint8_t k_Length_of_channel_message[15] =
{
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    3,  // 0x8  Note off
    3,  // 0x9  Note on
    3,  // 0xA  Aftertouch
    3,  // 0xB  Control change
    2,  // 0xC  Program change
    2,  // 0xD  Channel pressure
    3   // 0xE  Pitchbend
};

//==============================================================================

void MIDI_Init(void)
{
    g_Message.status = NULL;
    g_Message.index = 0;

    g_Mode = MIDI_MODE_NONE;
    g_PrevMode = MIDI_MODE_NONE;
    g_SystemExclusive = false;
    g_SysExIndex = 0;
    g_SysExChecksum = 0;
    g_RunningStatus = NULL;
    g_LearnedChannel = CHANNEL_NONE;

    Calibrate_Init();
}

void MIDI_SetMode(uint8_t mode)
{
    if (g_Mode != MIDI_MODE_CALIBRATE && g_Mode != mode)
    {
        g_Mode = mode;

        switch (g_Mode)
        {
        case MIDI_MODE_MONO:
            Mono_Init(&g_Voice[0]);
            break;
        case MIDI_MODE_POLY1:
            Poly1_Init(g_Voice);
            break;
        case MIDI_MODE_POLY4:
            Poly4_Init(g_Voice);
            break;
        }
    }
}

void SystemRealtime(uint8_t byte)
{
    switch (byte)
    {
    case MIDI_TIMING_CLOCK:
        Seq_ClockTick();
        break;
    case MIDI_START:
        Seq_ClockReset();
        Seq_Start();
        break;
    case MIDI_CONTINUE:
        Seq_Continue();
        break;
    case MIDI_STOP:
        Seq_Stop();
        break;
    case MIDI_RESET:
        // System reset
        asm("reset");
        break;
    }
}

static inline void ProcessMessage(void)
{
    uint8_t status = g_Message.status >> 4;
    uint8_t channel = g_Message.status & 0x0F;

    // Learn channel from first non-system message
    if (g_LearnedChannel == CHANNEL_NONE && status < 0xF)
    {
        g_LearnedChannel = channel;
    }

    // Only process messages on the learned channel (and system messages)
    if (g_Mode == MIDI_MODE_MONO && (channel == g_LearnedChannel || status == MIDI_STATUS_SYSTEM))
    {
        switch (status)
        {
        case MIDI_STATUS_NOTE_OFF:
            Mono_NoteOff(g_Message.data1, g_Message.data2);
            break;
        case MIDI_STATUS_NOTE_ON:
            if (g_Message.data1 == FUNCTION_KEY_CALIBRATE && g_Message.data2 > 0)
            {
                g_PrevMode = g_Mode;
                g_Mode = MIDI_MODE_CALIBRATE;
                Calibrate_Begin();
            }
            else
            {
                Mono_NoteOn(g_Message.data1, g_Message.data2);
            }
            break;
        case MIDI_STATUS_CONTROL_CHANGE:
            Mono_ControlChange(g_Message.data1, g_Message.data2);
            break;
        case MIDI_STATUS_CHANNEL_PRESSURE:
            Mono_ChannelPressure(g_Message.data1);
            break;
        case MIDI_STATUS_PITCHBEND:
            Mono_Pitchbend(g_Message.data1, g_Message.data2);
            break;
        }
    }
    else if (g_Mode == MIDI_MODE_POLY1 && (channel == g_LearnedChannel || status == MIDI_STATUS_SYSTEM))
    {
        switch (status)
        {
        case MIDI_STATUS_NOTE_OFF:
            Poly1_NoteOff(g_Message.data1, g_Message.data2);
            break;
        case MIDI_STATUS_NOTE_ON:
            if (g_Message.data1 == FUNCTION_KEY_CALIBRATE && g_Message.data2 > 0)
            {
                g_PrevMode = g_Mode;
                g_Mode = MIDI_MODE_CALIBRATE;
                Calibrate_Begin();
            }
            else
            {
                Poly1_NoteOn(g_Message.data1, g_Message.data2);
            }
            break;
        case MIDI_STATUS_CONTROL_CHANGE:
            Poly1_ControlChange(g_Message.data1, g_Message.data2);
            break;
        case MIDI_STATUS_CHANNEL_PRESSURE:
            Poly1_ChannelPressure(g_Message.data1);
            break;
        case MIDI_STATUS_PITCHBEND:
            Poly1_Pitchbend(g_Message.data1, g_Message.data2);
            break;
        }
    }
    else if (g_Mode == MIDI_MODE_POLY4 && ((channel >= g_LearnedChannel && channel < g_LearnedChannel + 4) || status == MIDI_STATUS_SYSTEM))
    {
        uint8_t which = channel - g_LearnedChannel;

        switch (status)
        {
        case MIDI_STATUS_NOTE_OFF:
            Poly4_NoteOff(which, g_Message.data1, g_Message.data2);
            break;
        case MIDI_STATUS_NOTE_ON:
            if (g_Message.data1 == FUNCTION_KEY_CALIBRATE && g_Message.data2 > 0)
            {
                g_PrevMode = g_Mode;
                g_Mode = MIDI_MODE_CALIBRATE;
                Calibrate_Begin();
            }
            else
            {
                Poly4_NoteOn(which, g_Message.data1, g_Message.data2);
            }
            break;
        case MIDI_STATUS_CONTROL_CHANGE:
            Poly4_ControlChange(which, g_Message.data1, g_Message.data2);
            break;
        case MIDI_STATUS_CHANNEL_PRESSURE:
            Poly4_ChannelPressure(which, g_Message.data1);
            break;
        case MIDI_STATUS_PITCHBEND:
            Poly4_Pitchbend(which, g_Message.data1, g_Message.data2);
            break;
        }
    }
    else if (g_Mode == MIDI_MODE_CALIBRATE)
    {
        if (status == MIDI_STATUS_NOTE_ON)
        {
            if (Calibrate_NoteOn(g_Message.data1, g_Message.data2))
            {
                if (EEPROM_SaveCalibration())
                {
                    LED_Blink(2);
                }
                g_Mode = g_PrevMode; // Return to previous mode when calibration is done
            }
        }
    }
}

static void ProcessSysEx(uint8_t *buffer, uint8_t length, uint8_t checksum)
{
    if (length < 5)
        return;

    if (buffer[length - 1] != MIDI_SYSEX_END)
        return;

    // Check IDs
    if (buffer[1] != SYSEX_MFG_EDU)
        return;
    if (buffer[2] != SYSEX_MIDI2CV_ID_1)
        return;
    if (buffer[3] != SYSEX_MIDI2CV_ID_2)
        return;

    // Verify checksum
    if (checksum != 0)
        return;

    // Check for specific messages
    if (length == 8 && buffer[4] == SYSEX_MSG_ID_LOCK_KEYS)
    {
        if (buffer[5] == 0 || buffer[5] == 1)
        {
            SETTINGS(persistent_lock) = buffer[5];
            EEPROM_SaveSettings();
        }
    }
    else if (length == 7 && buffer[4] == SYSEX_MSG_ID_RESET)
    {
        asm("reset");
    }
    else if (length == 7 && buffer[4] == SYSEX_MSG_ID_REPORT_VERSION)
    {
        LED_ReportVersion();
    }
}

void inline MIDI_Process(uint8_t byte)
{
    if (byte >= 0xF8) // System realtime message. 1 byte long. May be interleaved in a non-realtime message.
    {
        SystemRealtime(byte);
    }
    else if (byte == MIDI_SYSEX_BEGIN)
    {
        if (g_SysExIndex < MAX_SYSEX_LENGTH)
        {
            g_SysExBuffer[g_SysExIndex++] = byte;
            g_SysExChecksum = byte;
        }

        g_SystemExclusive = true;
        g_RunningStatus = NULL;
    }
    else if (g_SystemExclusive)
    {
        if (g_SysExIndex < MAX_SYSEX_LENGTH)
        {
            g_SysExBuffer[g_SysExIndex++] = byte;
            g_SysExChecksum += byte;
        }

        if (byte == MIDI_SYSEX_END)
        {
            ProcessSysEx(g_SysExBuffer, g_SysExIndex, g_SysExChecksum);
            g_SysExIndex = 0;
            g_SystemExclusive = false;
            g_RunningStatus = NULL;
        }
    }
    else
    {
        switch (g_Message.index)
        {
        case 0:
            g_Message.index++;

            if (byte & 0x80) // This is a status byte.
            {
                g_Message.status = byte;

                if (byte > 0xF0) // System common message (other than SysEx begin, which we already checked for).
                {
                    g_RunningStatus = NULL;

                    if (byte >= 0xF4) // This is a single-byte message.
                    {
                        // This is where a call to ProcessMessage() would go, but we
                        // don't care about any of these messages, so don't bother.
                        g_Message.index = 0;
                        break;
                    }

                    g_Message.length = k_Length_of_system_common_message[byte & 0x0F];
                }
                else // Channel voice or channel mode message.
                {
                    g_RunningStatus = byte;
                    g_Message.length = k_Length_of_channel_message[byte >> 4];
                }

                break;
            }
            else // We expected a status byte but didn't get one.
            {
                // If we don't have a running status, ignore this data.
                if (g_RunningStatus == NULL)
                    break;

                // If we do, set up the index and fall through to case 1.
                g_Message.index = 1;
            }
        case 1:
        case 2:
            g_Message.byte[g_Message.index] = byte;
            g_Message.index++;

            if (g_Message.index == g_Message.length)
            {
                ProcessMessage();
                g_Message.index = 0;
            }

            break;
        }
    }
}

inline void MIDI_Tick(void)
{
    switch (g_Mode)
    {
    case MIDI_MODE_MONO:
        Mono_Tick();
        break;

    case MIDI_MODE_POLY1:
        Poly1_Tick();
        break;

    case MIDI_MODE_POLY4:
        Poly4_Tick();
        break;
    }

    if (g_Mode != MIDI_MODE_CALIBRATE)
    {
        Seq_Tick();
    }
}

