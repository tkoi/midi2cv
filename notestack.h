#ifndef NOTESTACK_H_
#define NOTESTACK_H_

#include "midi2cv.h"

#define FREE_SLOT               0xFF
#define MAX_NOTESTACK_CAPACITY  8

typedef struct NoteEntry
{
    uint8_t note;
    uint8_t velocity;
    uint8_t next_index;  // 1-based
} NoteEntry_t;

typedef struct NoteStack
{
    NoteEntry_t pool[MAX_NOTESTACK_CAPACITY + 1];  // First element is a dummy node!
    uint8_t capacity;
    uint8_t pool_size;
    uint8_t top_index;  // Base 1.
} NoteStack_t;

void NoteStack_Init(NoteStack_t *ns, uint8_t capacity);
void NoteStack_NoteOn(NoteStack_t *ns, uint8_t note, uint8_t velocity);
void NoteStack_NoteOff(NoteStack_t *ns, uint8_t note);
void NoteStack_Clear(NoteStack_t *ns);

#endif

