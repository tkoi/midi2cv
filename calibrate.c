#include "calibrate.h"
#include "dac.h"
#include "eeprom.h"

/* CALIBRATION PROCEDURE
 * Enter calibration mode
 * For each CV output:
 *   FW writes DAC with 0x000
 *   Trim to 0V
 *   Press FN key "enter"
 *   FW writes DAC with estimate for 9 volts
 *   Trim to 9 volts
 *   Press FN key "enter"
 *   For V = {1, 2, ... 8}
 *     FW writes DAC with estimate for V
 *     Use FN keys to trim to V
 *     Press FN key "enter"
 *   FW generates a V->DAC LUT for V = {0, 1, ... 9}
 * FW writes LUTs to EEPROM
 */

#define NOTE_NEXT_CV            12 // C1
#define NOTE_TRIM_COARSE_DOWN   14 // D1
#define NOTE_TRIM_FINE_DOWN     16 // E1
#define NOTE_TRIM_FINE_UP       17 // F1
#define NOTE_TRIM_COARSE_UP     19 // G1
#define NOTE_CALIBRATE          20 // G#1
#define NOTE_ENTER              21 // A1

#define MAX_VOLTAGE_TO_TRIM     8 ///< In volts

#define STATE_BEGIN             0
#define STATE_ZERO_SCALE        1
#define STATE_FULL_SCALE        2
#define STATE_TRIM_VOLTAGE      3

// Mathematica: Table[v*32*12,{v,0,9}]
static const uint16_t k_VoltageToDACBase[CALIBRATE_LUT_SIZE] =
{
    0, 384, 768, 1152, 1536, 1920, 2304, 2688, 3072, 3456
};

#define OFFSET_MAX          127
#define OFFSET_MIN          -128

static int16_t g_CurrentOffset;
static uint8_t g_WhichCV; ///< The CV out we are currently calibrating
static uint8_t g_WhichVoltage; ///< The voltage we are calibrating for this CV
static uint8_t g_State;

// 16-bit fixed point
#define FIXED_POINT_WIDTH 16
static const uint16_t k_FractionalVoltageToFixedPoint[12] =
{
    0x0000, 0x1556, 0x2AAB, 0x4000,
    0x5556, 0x6AAB, 0x8000, 0x9556,
    0xAAAB, 0xC000, 0xD556, 0xEAAB
};

static void FinishCalibration(void);
static void IndicateWithGate(uint8_t state);

//==============================================================================

void Calibrate_Init(void)
{
    g_WhichCV = 0;
    g_WhichVoltage = 0;
    g_State = STATE_BEGIN;
}

void Calibrate_Begin(void)
{
    Calibrate_Init();
    Calibrate_NoteOn(NOTE_ENTER, 1);
}

/** \brief Process key presses during calibration.
 *  \param note MIDI note number.
 *  \param velocity MIDI note velocity.
 *  \return True if calibration is finished, false if not.
 */
bool_t Calibrate_NoteOn(uint8_t note, uint8_t velocity)
{
    if (velocity == 0)
        return false;
    
    if (note == NOTE_CALIBRATE) // Abort calibration
    {
        IndicateWithGate(0);
        FinishCalibration();
        return true;
    }
    
    uint8_t step = 1;
    
    switch (note)
    {
    case NOTE_TRIM_COARSE_DOWN:
        step = 10;
    case NOTE_TRIM_FINE_DOWN:
        if (g_State == STATE_TRIM_VOLTAGE)
        {
            if (g_CurrentOffset > OFFSET_MIN + step)
                g_CurrentOffset -= step;
            else
                g_CurrentOffset = OFFSET_MIN;
            DAC_Set(g_WhichCV, k_VoltageToDACBase[g_WhichVoltage] + g_CurrentOffset);
        }
        break;
    
    case NOTE_TRIM_COARSE_UP:
        step = 10;
    case NOTE_TRIM_FINE_UP:
        if (g_State == STATE_TRIM_VOLTAGE)
        {
            if (g_CurrentOffset < OFFSET_MAX - step)
                g_CurrentOffset += step;
            else
                g_CurrentOffset = OFFSET_MAX;
            DAC_Set(g_WhichCV, k_VoltageToDACBase[g_WhichVoltage] + g_CurrentOffset);
        }
        break;
    
    case NOTE_ENTER:
        switch (g_State)
        {
        case STATE_BEGIN:
            g_State = STATE_ZERO_SCALE;
            DAC_Set(g_WhichCV, DAC_ZERO_SCALE); // Trim to 0V
            IndicateWithGate(1);
            break;
        
        case STATE_ZERO_SCALE:
            CALIBRATION(g_WhichCV, 0) = 0;
            g_State = STATE_FULL_SCALE;
            DAC_Set(g_WhichCV, k_VoltageToDACBase[9]); // Trim to 9V
            break;
        
        case STATE_FULL_SCALE:
            g_State = STATE_TRIM_VOLTAGE;
            g_WhichVoltage = 1;
            g_CurrentOffset = CALIBRATION(g_WhichCV, g_WhichVoltage);
            DAC_Set(g_WhichCV, k_VoltageToDACBase[g_WhichVoltage] + g_CurrentOffset);
            break;
            
        case STATE_TRIM_VOLTAGE:
            CALIBRATION(g_WhichCV, g_WhichVoltage) = g_CurrentOffset;
            if (g_WhichVoltage == MAX_VOLTAGE_TO_TRIM)
            {
                IndicateWithGate(0);
                if (g_WhichCV == CALIBRATE_NUM_CV_OUTS - 1)
                {
                    FinishCalibration();
                    return true; // Done calibrating
                }
                else
                {
                    g_WhichCV++;
                    g_State = STATE_ZERO_SCALE;
                    DAC_Set(g_WhichCV, DAC_ZERO_SCALE);
                    IndicateWithGate(1);
                }
            }
            else
            {
                g_WhichVoltage++;
                g_CurrentOffset = CALIBRATION(g_WhichCV, g_WhichVoltage);
                DAC_Set(g_WhichCV, k_VoltageToDACBase[g_WhichVoltage] + g_CurrentOffset);
            }
            break;
        }
        break;
    
    case NOTE_NEXT_CV:
        IndicateWithGate(0);
        if (g_WhichCV == CALIBRATE_NUM_CV_OUTS - 1)
        {
            FinishCalibration();
            return true; // Done calibrating
        }
        else
        {
            g_WhichCV++;
            g_State = STATE_ZERO_SCALE;
            DAC_Set(g_WhichCV, DAC_ZERO_SCALE);
            IndicateWithGate(1);
        }
        break;
    }
    
    return false;
}

/** \brief Get a calibrated DAC code from a MIDI note number.
 *  \param note MIDI note number.
 *  \return The calibrated DAC code.
 */
uint16_t Calibrate_GetCalibratedPitch(uint8_t dac, uint8_t note)
{
    // MIDI note 24 corresponds to 0 V
    // An increment of the MIDI note corresponds to an increment of 1/12 V
    
    // Floor division by 12 gives the integral part of voltage
    // The remainder is the fractional part in units of 1/12 volt
    uint8_t integral = 0;
    uint8_t fractional = (note < 24) ? 0 : note - 24;
    
    while (fractional >= 12)
    {
        fractional -= 12;
        integral++;
    }
    
    // Interpolate to find the calibrated DAC code
    if (fractional == 0)
    {
        return k_VoltageToDACBase[integral] + CALIBRATION(dac, integral);
    }
    else
    {
        int32_t delta = k_VoltageToDACBase[integral + 1]
                      + CALIBRATION(dac, integral + 1)
                      - k_VoltageToDACBase[integral]
                      - CALIBRATION(dac, integral);
        
        // Multiply by fractional/12 to find the fractional DAC code
        delta *= k_FractionalVoltageToFixedPoint[fractional];
        delta >>= FIXED_POINT_WIDTH;
        
        return k_VoltageToDACBase[integral] + CALIBRATION(dac, integral) + delta;
    }
}

//==============================================================================

static void FinishCalibration(void)
{
    // Extrapolate the last value in each LUT
    // for (uint8_t i = 0; i < CALIBRATE_NUM_CV_OUTS; i++)
    // {
        // int16_t delta = CALIBRATION(i, MAX_VOLTAGE_TO_TRIM)
                           // - CALIBRATION(i, MAX_VOLTAGE_TO_TRIM - 1);
        
        // CALIBRATION(i, MAX_VOLTAGE_TO_TRIM + 1) = CALIBRATION(i, MAX_VOLTAGE_TO_TRIM) + delta;
    // }
}

/** \brief Use the gate LEDs to indicate which CV is currently being calibrated.
 *  \param state 0 to turn off the LED or 1 to turn it on.
 *  \note The value of \c g_WhichCV decides which LED is affected.
 */
static void IndicateWithGate(uint8_t state)
{
    di();

    if (state)
    {
        MIDI2CV_PinSet(k_GATE_LUT[g_WhichCV]);
    }
    else
    {
        MIDI2CV_PinClear(k_GATE_LUT[g_WhichCV]);
    }
    
    ei();
}

