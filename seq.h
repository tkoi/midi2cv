#ifndef SEQ_H_
#define SEQ_H_

#include "midi2cv.h"

void Seq_Init(pin_t syncPin, pin_t runPin, pin_t resetPin);

void Seq_IncrementDiv2(void);
void Seq_ToggleDiv3(void);

void Seq_ClockTick(void);
void Seq_ClockReset(void);

void Seq_Start(void);
void Seq_Continue(void);
void Seq_Stop(void);

inline void Seq_Tick(void);


#endif

