#include "midi2cv.h"
#include "mono.h"
#include "eeprom.h"
#include "led.h"
#include "seq.h"

#define NOTE_CV2_LEARN  12 // C1.  Enters CV2 learn mode
#define NOTE_DIV3       13 // C#1. Toggles software divider between /1 and /3
#define NOTE_RETRIGGER  14 // D1.  Toggles retrigger
#define NOTE_DIV2       15 // D#1. Selects software /2 divider state thru /1, /2, /4 and /8
#define NOTE_EXP_VEL    16 // E1.  Toggles exponential velocity
#define NOTE_REGATE     18 // F#1. Toggles regate
#define NOTE_CHPRESSURE 19 // G1.  Assigns Aftertouch to CV2
#define NOTE_CALIBRATE  20 // G#1. Starts software calibration procedure
#define NOTE_LOCK       22 // A#1. Enables lock of the function keys till next restart
#define NOTE_SAVE       23 // B1.  Saves parameters.

static Voice_t *g_Voice;
static bool_t g_FunctionKeysAreLocked;
static bool_t g_CV2Learn;

//==============================================================================

void Mono_Init(Voice_t *voice)
{
    g_Voice = voice;
    g_FunctionKeysAreLocked = false;
    g_CV2Learn = false;

    Voice_Init(voice, MAX_NOTESTACK_CAPACITY, DAC_A, DAC_C, DAC_D, DAC_B, GATE_A, GATE_B);
    Seq_Init(GATE_SYNC, GATE_C, GATE_D);
    LED_Init();
}

void Mono_NoteOn(uint8_t note, uint8_t velocity)
{
    if (note >= 24)
    {
        if (g_CV2Learn == true)
        {
            SETTINGS(cv2_mode) = SETTINGS_CV2_MODE_NOTE;
            g_CV2Learn = false;
            LED_Unforce();
        }

        Voice_NoteOn(g_Voice, note, velocity);
    }
    if (SETTINGS(persistent_lock) == false && g_FunctionKeysAreLocked == false && velocity > 0)
    {
        // Cancel CV2 learn mode if another function key is pressed
        g_CV2Learn = false;
        LED_Unforce();
        
        switch (note)
        {
        case NOTE_CV2_LEARN:
            g_CV2Learn = true;
            LED_Force(LED_ON);
            break;
        case NOTE_DIV3:
            Seq_ToggleDiv3();
            break;
        case NOTE_RETRIGGER:
            SETTINGS(retrigger) = !SETTINGS(retrigger);
            break;
        case NOTE_DIV2:
            Seq_IncrementDiv2();
            break;
        case NOTE_EXP_VEL:
            SETTINGS(exp_velocity) = !SETTINGS(exp_velocity);
            break;
        case NOTE_REGATE:
            SETTINGS(regate) = !SETTINGS(regate);
            break;
        case NOTE_CHPRESSURE:
            SETTINGS(cv2_mode) = SETTINGS_CV2_MODE_CHPRESSURE;
            break;
        case NOTE_LOCK:
            g_FunctionKeysAreLocked = true;
            break;
        case NOTE_SAVE:
            if (EEPROM_SaveSettings())
            {
                LED_Blink(2);
            }
            break;
        }
    }
}

void Mono_NoteOff(uint8_t note, uint8_t velocity)
{
    Voice_NoteOff(g_Voice, note, velocity);
}

void Mono_Pitchbend(uint8_t lsbits, uint8_t msbits)
{
    Voice_Pitchbend(g_Voice, lsbits, msbits);
}

void Mono_ControlChange(uint8_t control, uint8_t value)
{
    // First check if this is a channel mode message
    if (control >= 120)
    {
        if (control == MIDI_CC_RESET_ALL_CONTROLLERS && value == 0)
        {
            g_CV2Learn = false;
            LED_Unforce();
        }
    }
    else if (g_CV2Learn == true)
    {
        SETTINGS(cv2_mode) = SETTINGS_CV2_MODE_CC;
        SETTINGS(cv2_cc) = control;
        g_CV2Learn = false;
        LED_Unforce();
    }

    Voice_ControlChange(g_Voice, control, value);
}

void Mono_ChannelPressure(uint8_t pressure)
{
    Voice_ChannelPressure(g_Voice, pressure);
}

inline void Mono_Tick(void)
{
    Voice_Tick(g_Voice);
}

