#ifndef DAC_H_
#define DAC_H_

#include "midi2cv.h"

#define DAC_ZERO_SCALE      0x000
#define DAC_FULL_SCALE      0xFFF

void DAC_Init(void);
void DAC_Set(dac_t dac, uint16_t code);

#endif

