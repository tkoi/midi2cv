#include "midi2cv.h"
#include "poly4.h"
#include "eeprom.h"
#include "led.h"
#include "seq.h"

#define NOTE_EXP_VEL    16 // E1.  Toggles exponential velocity
#define NOTE_REGATE     18 // F#1. Toggles regate
#define NOTE_CALIBRATE  20 // G#1. Starts software calibration procedure
#define NOTE_LOCK       22 // A#1. Enables lock of the function keys till next restart
#define NOTE_SAVE       23 // B1.  Saves parameters.

static bool_t g_FunctionKeysAreLocked;
static Voice_t *g_pVoices;

//==============================================================================

void Poly4_Init(Voice_t *voices)
{
    g_pVoices = voices;

    for (uint8_t i = 0; i < 4; i++)
    {
        Voice_Init(&g_pVoices[i], MAX_NOTESTACK_CAPACITY, k_DAC_LUT[i], DAC_NONE, DAC_NONE, DAC_NONE, k_GATE_LUT[i], GATE_NONE);
    }

    Seq_Init(GATE_SYNC, GATE_NONE, GATE_NONE);
    LED_Init();
    
    g_FunctionKeysAreLocked = false;
}

void Poly4_NoteOn(uint8_t which, uint8_t note, uint8_t velocity)
{
    if (note >= 24)
    {
        Voice_NoteOn(&g_pVoices[which], note, velocity);
    }
    else if (SETTINGS(persistent_lock) == false && g_FunctionKeysAreLocked == false && velocity > 0)
    {
        switch (note)
        {
        case NOTE_EXP_VEL:
            SETTINGS(exp_velocity) = !SETTINGS(exp_velocity);
            break;
        case NOTE_REGATE:
            SETTINGS(regate) = !SETTINGS(regate);
            break;
        case NOTE_LOCK:
            g_FunctionKeysAreLocked = true;
            break;
        case NOTE_SAVE:
            if (EEPROM_SaveSettings())
            {
                LED_Blink(2);
            }
            break;
        }
    }
}

void Poly4_NoteOff(uint8_t which, uint8_t note, uint8_t velocity)
{
    Voice_NoteOff(&g_pVoices[which], note, velocity);
}

void Poly4_Pitchbend(uint8_t which, uint8_t lsbits, uint8_t msbits)
{
    Voice_Pitchbend(&g_pVoices[which], lsbits, msbits);
}

void Poly4_ControlChange(uint8_t which, uint8_t control, uint8_t value)
{
    Voice_ControlChange(&g_pVoices[which], control, value);
}

void Poly4_ChannelPressure(uint8_t which, uint8_t pressure)
{
    Voice_ChannelPressure(&g_pVoices[which], pressure);
}

inline void Poly4_Tick(void)
{
    for (uint8_t i = 0; i < 4; i++)
    {
        Voice_Tick(&g_pVoices[i]);
    }
}

