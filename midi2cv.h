#ifndef MIDI2CV_H_
#define MIDI2CV_H_

#include <xc.h>

#define _XTAL_FREQ      32000000
#define NULL            0

#define MIDI_CC_MOD                     1
#define MIDI_CC_SUSTAIN                 64
#define MIDI_CC_ALL_SOUND_OFF           120
#define MIDI_CC_RESET_ALL_CONTROLLERS   121
#define MIDI_CC_ALL_NOTES_OFF           123

#define PITCHBEND_MIDPOINT  64
#define PULSE_DURATION      2 // Milliseconds, approximately

#define GATE_SYNC   PIN_A0
#define GATE_A      PIN_B7
#define GATE_B      PIN_B6
#define GATE_C      PIN_B4
#define GATE_D      PIN_B3
#define GATE_NONE   PIN_NONE

typedef enum
{
    DAC_A = 0,
    DAC_B = 1,
    DAC_C = 2,
    DAC_D = 3,
    DAC_NONE = 0xFF
} dac_t;

typedef enum
{
    // bits [5:4] port select
    // bits [2:0] pin number
    PIN_NONE = 0,
    PIN_A0 = 0x10,
    PIN_A1 = 0x11,
    PIN_A2 = 0x12,
    PIN_A3 = 0x13,
    PIN_A4 = 0x14,
    PIN_A5 = 0x15,
    PIN_A6 = 0x16,
    PIN_A7 = 0x17,
    PIN_B0 = 0x20,
    PIN_B1 = 0x21,
    PIN_B2 = 0x22,
    PIN_B3 = 0x23,
    PIN_B4 = 0x24,
    PIN_B5 = 0x25,
    PIN_B6 = 0x26,
    PIN_B7 = 0x27,
} pin_t;

const dac_t k_DAC_LUT[4] = {DAC_A, DAC_B, DAC_C, DAC_D};
const pin_t k_GATE_LUT[4] = {GATE_A, GATE_B, GATE_C, GATE_D};

typedef enum {false = 0, true = 1} bool_t;
typedef unsigned char uint8_t;
typedef signed char int8_t;
typedef unsigned short uint16_t;
typedef signed short int16_t;
typedef unsigned long uint32_t;
typedef signed long int32_t;


void MIDI2CV_PinSet(pin_t pin);

void MIDI2CV_PinClear(pin_t pin);


#endif

