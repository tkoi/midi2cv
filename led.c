#include "led.h"

#define LED_PIN     LATA7

static uint8_t g_UnforcedState;
bool_t g_LedIsForced;

void LED_Init(void)
{
    LED_Set(LED_OFF);
    LED_Unforce();
}

void LED_Set(uint8_t state)
{
    g_UnforcedState = state;
    
    di();
    if (g_LedIsForced == false)
    {
        LED_PIN = state;
    }
    ei();
}

void LED_Force(uint8_t state)
{
    g_LedIsForced = true;
    
    di();
    LED_PIN = state;
    ei();
}

void LED_Unforce(void)
{
    g_LedIsForced = false;
    
    di();
    LED_PIN = g_UnforcedState;
    ei();
}

void LED_Blink(uint8_t t)
{
    uint8_t i;
    
    LED_Force(LED_OFF);
        
    for (i = 0; i < t; i++)
    {
        LED_Force(LED_ON);
        __delay_ms(125);
        LED_Force(LED_OFF);
        __delay_ms(125);
    }
    
    LED_Unforce();
}

void LED_ReportVersion(void)
{
    uint16_t code = VERSION_CODE;

    for (uint8_t i = 0; i < 4; i++)
    {
        LED_PIN = 1;
        (code & 0x8000) ? MIDI2CV_PinSet(GATE_A) : MIDI2CV_PinClear(GATE_A);
        (code & 0x4000) ? MIDI2CV_PinSet(GATE_B) : MIDI2CV_PinClear(GATE_B);
        (code & 0x2000) ? MIDI2CV_PinSet(GATE_C) : MIDI2CV_PinClear(GATE_C);
        (code & 0x1000) ? MIDI2CV_PinSet(GATE_D) : MIDI2CV_PinClear(GATE_D);
        code <<= 4;

        __delay_ms(500);

        LED_PIN = 0;
        MIDI2CV_PinClear(GATE_A);
        MIDI2CV_PinClear(GATE_B);
        MIDI2CV_PinClear(GATE_C);
        MIDI2CV_PinClear(GATE_D);

        __delay_ms(125);
    }
}

