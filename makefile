
ASSEMBLER   := "/c/Program Files (x86)/Microchip/MPLABX/v3.55/mpasmx/mpasmx.exe"
LINKER   	:= "/c/Program Files (x86)/Microchip/MPLABX/v3.55/mpasmx/mplink.exe"
COMPILER    := xc8
HEXMATE     := hexmate
HEX2MIDI    := py hex2midi.py
VERSION     := 0x0022
CHIP        := 16F1847
MSGDISABLE  := 1352

BUILD_DIR	:= build

BL_SOURCE   := bootloader.asm
BL_LST      := $(BUILD_DIR)/bootloader.lst
BL_ERR      := $(BUILD_DIR)/bootloader.err
APP_HEX     := $(BUILD_DIR)/main.hex
MERGE_HEX   := $(BUILD_DIR)/midi2cv.hex
MIDI_FILE   := $(BUILD_DIR)/midi2cv.mid
BL_OBJ		:= $(BUILD_DIR)/$(BL_SOURCE:.asm=.o)
BL_HEX      := $(BUILD_DIR)/$(BL_SOURCE:.asm=.hex)
DEP_FILE	:= $(BUILD_DIR)/depends.mk

C_FILES     := $(wildcard *.c)
O_FILES		:= $(addprefix $(BUILD_DIR)/, $(C_FILES:.c=.o))
D_FILES		:= $(addprefix $(BUILD_DIR)/, $(C_FILES:.c=.d))
DEP_FILES	:= $(addprefix $(BUILD_DIR)/, $(C_FILES:.c=.dep))
P1_FILES	:= $(addprefix $(BUILD_DIR)/, $(C_FILES:.c=.p1))
PRE_FILES	:= $(addprefix $(BUILD_DIR)/, $(C_FILES:.c=.pre))

CFLAGS		:= --chip=$(CHIP) --opt=all -g --MSGDISABLE=$(MSGDISABLE) -DVERSION_CODE=$(VERSION)

APP_FILES	:= $(wildcard $(BUILD_DIR)/main.*)
STARTUP_FILES := $(wildcard $(BUILD_DIR)/startup.*)
HEX2MIDI_LOG := $(BUILD_DIR)/hex2midi.log

$(BUILD_DIR)/%.d: %.c | $(BUILD_DIR)
	@echo Writing $@
	@$(COMPILER) -q $(CFLAGS) --scandep -o$@ $<

$(BUILD_DIR)/%.p1: %.c | $(BUILD_DIR)
	@echo Compiling $<
	@$(COMPILER) -q $(CFLAGS) --pass1 $< -o$@

all:    hex midi
bl:     $(BL_HEX)
hex:    $(MERGE_HEX)
midi:   $(MIDI_FILE)
objs:	$(O_FILES)
depends: $(DEP_FILE)

$(DEP_FILE): $(D_FILES) | $(BUILD_DIR)
	@echo Writing $(DEP_FILE)
	@awk '{print}' $(D_FILES) > $(DEP_FILE)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BL_OBJ): bootloader.asm | $(BUILD_DIR)
	@echo Assembling $@
	@$(ASSEMBLER) -s -q -e$(BL_ERR) -l$(BL_LST) -o$(BL_OBJ) -p$(CHIP) bootloader.asm

$(BL_HEX): $(BL_OBJ)
	@echo Linking $@
	@$(LINKER) -q -p$(CHIP) $(BL_OBJ) -o$(BL_HEX)

$(APP_HEX): $(P1_FILES) | $(BUILD_DIR)
	@echo Linking $@
	@$(COMPILER) -q $(CFLAGS) -m -g --codeoffset=0x200 -O$(APP_HEX) $(P1_FILES)

$(MERGE_HEX): $(BL_HEX) $(APP_HEX) | $(BUILD_DIR)
	@echo Merging $@
	@$(HEXMATE) $(BL_HEX) +$(APP_HEX) -O$(MERGE_HEX) -size

$(MIDI_FILE): $(APP_HEX) | $(BUILD_DIR)
	@echo Writing $@
	@$(HEX2MIDI) -i $(APP_HEX) -o $(MIDI_FILE) -l $(HEX2MIDI_LOG)

clean:
	rm -f $(DEP_FILE) $(DEP_FILES)
	rm -f $(BL_LST) $(BL_ERR) $(BL_OBJ)
	rm -f $(O_FILES) $(D_FILES) $(P1_FILES) $(PRE_FILES)
	rm -f $(APP_FILES) $(STARTUP_FILES)
	rm -f $(BL_HEX) $(APP_HEX) $(MERGE_HEX) $(MIDI_FILE) $(HEX2MIDI_LOG)

.PHONY: all clean bl hex midi objs depends

-include $(DEP_FILE)
