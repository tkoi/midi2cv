#ifndef POLY1_H_
#define POLY1_H_

#include "midi2cv.h"
#include "voice.h"

void Poly1_Init(Voice_t *voices);
void Poly1_NoteOn(uint8_t note, uint8_t velocity);
void Poly1_NoteOff(uint8_t note, uint8_t velocity);
void Poly1_Pitchbend(uint8_t lsbits, uint8_t msbits);
void Poly1_ChannelPressure(uint8_t pressure);
void Poly1_ControlChange(uint8_t control, uint8_t value);
inline void Poly1_Tick(void);


#endif

