#ifndef LED_H_
#define LED_H_

#include "midi2cv.h"

#define LED_ON  1
#define LED_OFF 0

void LED_Init(void);
void LED_Set(uint8_t state);
void LED_Force(uint8_t state);
void LED_Unforce(void);
void LED_Blink(uint8_t t);
void LED_ReportVersion(void);


#endif

