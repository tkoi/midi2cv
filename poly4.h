#ifndef POLY4_H_
#define POLY4_H_

#include "midi2cv.h"
#include "voice.h"

void Poly4_Init(Voice_t *voices);
void Poly4_NoteOn(uint8_t which, uint8_t note, uint8_t velocity);
void Poly4_NoteOff(uint8_t which, uint8_t note, uint8_t velocity);
void Poly4_Pitchbend(uint8_t which, uint8_t lsbits, uint8_t msbits);
void Poly4_ChannelPressure(uint8_t which, uint8_t pressure);
void Poly4_ControlChange(uint8_t which, uint8_t control, uint8_t value);
inline void Poly4_Tick(void);


#endif

