#ifndef MIDI_H_
#define MIDI_H_

#include "midi2cv.h"

#define MIDI_MODE_NONE      0
#define MIDI_MODE_POLY1     1
#define MIDI_MODE_MONO      2
#define MIDI_MODE_POLY4     3
#define MIDI_MODE_CALIBRATE 4

void MIDI_Init(void);
void MIDI_SetMode(uint8_t mode);
void inline MIDI_Process(uint8_t byte);
inline void MIDI_Tick(void);


#endif

