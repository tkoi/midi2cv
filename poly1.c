// Copyright 2012 Olivier Gillet.
//
// Author: Olivier Gillet (ol.gillet@gmail.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// See http://creativecommons.org/licenses/MIT/ for more information.
//
// -----------------------------------------------------------------------------
//
// Polyphonic voice allocator.

#include "midi2cv.h"
#include "poly1.h"
#include "eeprom.h"
#include "led.h"
#include "seq.h"

#define CAPACITY        8

#define NOTE_DIV3       13 // C#1. Toggles software divider between /1 and /3
#define NOTE_DIV2       15 // D#1. Selects software /2 divider state thru /1, /2, /4 and /8
#define NOTE_EXP_VEL    16 // E1.  Toggles exponential velocity
#define NOTE_POLY_LIMIT 17 // F1.  Changes polyphony limit
#define NOTE_REGATE     18 // F#1. Toggles regate
#define NOTE_CALIBRATE  20 // G#1. Starts software calibration procedure
#define NOTE_LOCK       22 // A#1. Enables lock of the function keys till next restart
#define NOTE_SAVE       23 // B1.  Saves parameters.

static bool_t g_FunctionKeysAreLocked;
static Voice_t *g_pVoices;
static uint8_t g_NumVoices;

#define NOT_ALLOCATED   0xFF
#define ACTIVE_NOTE     0x80
#define NOTE_MASK       0x7F

static uint8_t g_Pool[CAPACITY];
// Holds the indices of the voices sorted by most recent usage.
static uint8_t g_LRU[CAPACITY];

//==============================================================================

static void Touch(uint8_t voice)
{
    int8_t source = CAPACITY - 1;
    int8_t destination = CAPACITY - 1;

    while (source >= 0)
    {
        if (g_LRU[source] != voice)
        {
            g_LRU[destination--] = g_LRU[source];
        }

        --source;
    }

    g_LRU[0] = voice;
}

static uint8_t Find(uint8_t note)
{
    uint8_t i;
    
    for (i = 0; i < g_NumVoices; ++i)
    {
        if ((g_Pool[i] & NOTE_MASK) == note)
        {
            return i;
        }
    }

    return NOT_ALLOCATED;
}

static void Clear()
{
    uint8_t i;
    
    for (i = 0; i < CAPACITY; ++i)
    {
        g_Pool[i] = 0;
        g_LRU[i] = CAPACITY - i - 1;
    }
}

//==============================================================================

void Poly1_Init(Voice_t *voices)
{
    g_pVoices = voices;
    g_NumVoices = SETTINGS(num_voices);

    switch (g_NumVoices)
    {
    //                               pitch     vel       mod       learn     gate       trig
    case 2:
        Voice_Init(&g_pVoices[0], 1, DAC_A,    DAC_C,    DAC_NONE, DAC_NONE, GATE_A,    GATE_NONE);
        Voice_Init(&g_pVoices[1], 1, DAC_B,    DAC_D,    DAC_NONE, DAC_NONE, GATE_B,    GATE_NONE);
        Voice_Init(&g_pVoices[2], 1, DAC_NONE, DAC_NONE, DAC_NONE, DAC_NONE, GATE_NONE, GATE_NONE);
        Voice_Init(&g_pVoices[3], 1, DAC_NONE, DAC_NONE, DAC_NONE, DAC_NONE, GATE_NONE, GATE_NONE);
        Seq_Init(GATE_SYNC, GATE_C, GATE_D);
        break;
    case 3:
        Voice_Init(&g_pVoices[0], 1, DAC_A,    DAC_NONE, DAC_D,    DAC_NONE, GATE_A,    GATE_NONE);
        Voice_Init(&g_pVoices[1], 1, DAC_B,    DAC_NONE, DAC_NONE, DAC_NONE, GATE_B,    GATE_NONE);
        Voice_Init(&g_pVoices[2], 1, DAC_C,    DAC_NONE, DAC_NONE, DAC_NONE, GATE_C,    GATE_NONE);
        Voice_Init(&g_pVoices[3], 1, DAC_NONE, DAC_NONE, DAC_NONE, DAC_NONE, GATE_NONE, GATE_NONE);
        Seq_Init(GATE_SYNC, GATE_NONE, GATE_D);
        break;
    case 4:
        Voice_Init(&g_pVoices[0], 1, DAC_A, DAC_NONE, DAC_NONE, DAC_NONE, GATE_A, GATE_NONE);
        Voice_Init(&g_pVoices[1], 1, DAC_B, DAC_NONE, DAC_NONE, DAC_NONE, GATE_B, GATE_NONE);
        Voice_Init(&g_pVoices[2], 1, DAC_C, DAC_NONE, DAC_NONE, DAC_NONE, GATE_C, GATE_NONE);
        Voice_Init(&g_pVoices[3], 1, DAC_D, DAC_NONE, DAC_NONE, DAC_NONE, GATE_D, GATE_NONE);
        Seq_Init(GATE_SYNC, GATE_NONE, GATE_NONE);
        break;
    }

    LED_Init();

    g_FunctionKeysAreLocked = false;
    
    Clear();
}

void Poly1_NoteOn(uint8_t note, uint8_t velocity)
{
    if (note >= 24)
    {
        if (velocity == 0)
        {
            Poly1_NoteOff(note, velocity);
        }
        else
        {
            if (g_NumVoices == 0)
            {
                return;
            }

            // First, check if there is a voice currently playing this note. In this
            // case, this voice will be responsible for retriggering this note.
            // Hint: if you're more into string instruments than keyboard instruments,
            // you can safely comment those lines.
            uint8_t voice = Find(note);
            
            uint8_t i;

            // Then, try to find the least recently touched, currently inactive voice.
            if (voice == NOT_ALLOCATED)
            {
                for (i = 0; i < CAPACITY; ++i)
                {
                    if (g_LRU[i] < g_NumVoices && !(g_Pool[g_LRU[i]] & ACTIVE_NOTE))
                    {
                        voice = g_LRU[i];
                    }
                }
            }

            // If all voices are active, use the least recently played note
            // (voice-stealing).
            if (voice == NOT_ALLOCATED)
            {
                for (i = 0; i < CAPACITY; ++i)
                {
                    if (g_LRU[i] < g_NumVoices)
                    {
                        voice = g_LRU[i];
                    }
                }
            }

            g_Pool[voice] = note | ACTIVE_NOTE;
            Touch(voice);
            Voice_NoteOn(&g_pVoices[voice], note, velocity);
        }
    }
    else if (SETTINGS(persistent_lock) == false && g_FunctionKeysAreLocked == false && velocity > 0)
    {
        switch (note)
        {
        case NOTE_DIV3:
            Seq_ToggleDiv3();
            break;
        case NOTE_DIV2:
            Seq_IncrementDiv2();
            break;
        case NOTE_EXP_VEL:
            SETTINGS(exp_velocity) = !SETTINGS(exp_velocity);
            break;
        case NOTE_POLY_LIMIT:
            if (SETTINGS(stack_mode) == SETTINGS_STACK_MODE_NONE)
            {
                switch (SETTINGS(num_voices))
                {
                    case 2:  g_NumVoices = 3; break;
                    case 3:  g_NumVoices = 4; break;
                    case 4:  g_NumVoices = 2; break;
                    default: g_NumVoices = 4;
                }
                
                SETTINGS(num_voices) = g_NumVoices;
                Poly1_Init(g_pVoices);
                
            }
            break;
        case NOTE_REGATE:
            SETTINGS(regate) = !SETTINGS(regate);
            break;
        case NOTE_LOCK:
            g_FunctionKeysAreLocked = true;
            break;
        case NOTE_SAVE:
            if (EEPROM_SaveSettings())
            {
                LED_Blink(2);
            }
            break;
        }
    }
}

void Poly1_NoteOff(uint8_t note, uint8_t velocity)
{
    uint8_t voice = Find(note);

    if (voice != NOT_ALLOCATED)
    {
        g_Pool[voice] &= NOTE_MASK;
        Touch(voice);
    }

    for (uint8_t i = 0; i < g_NumVoices; i++)
    {
        Voice_NoteOff(&g_pVoices[voice], note, velocity);
    }
}

void Poly1_Pitchbend(uint8_t lsbits, uint8_t msbits)
{
    for (uint8_t i = 0; i < g_NumVoices; i++)
    {
        Voice_Pitchbend(&g_pVoices[i], lsbits, msbits);
    }
}

void Poly1_ChannelPressure(uint8_t pressure)
{
    for (uint8_t i = 0; i < g_NumVoices; i++)
    {
        Voice_ChannelPressure(&g_pVoices[i], pressure);
    }
    
}

void Poly1_ControlChange(uint8_t control, uint8_t value)
{
    for (uint8_t i = 0; i < g_NumVoices; i++)
    {
        Voice_ControlChange(&g_pVoices[i], control, value);
    }
}

inline void Poly1_Tick(void)
{
    for (uint8_t i = 0; i < g_NumVoices; i++)
    {
        Voice_Tick(&g_pVoices[i]);
    }
}

