#ifndef EEPROM_H_
#define EEPROM_H_

#define SETTINGS_CV2_MODE_NONE          0
#define SETTINGS_CV2_MODE_NOTE          1
#define SETTINGS_CV2_MODE_CC            2
#define SETTINGS_CV2_MODE_CHPRESSURE    3

#define SETTINGS_STACK_MODE_NONE        0
#define SETTINGS_STACK_MODE_UNIT1       1
#define SETTINGS_STACK_MODE_UNIT2       2

#define SETTINGS_DIVIDER_1              0
#define SETTINGS_DIVIDER_2              2
#define SETTINGS_DIVIDER_3              1
#define SETTINGS_DIVIDER_4              4
#define SETTINGS_DIVIDER_6              3
#define SETTINGS_DIVIDER_8              6
#define SETTINGS_DIVIDER_12             5
#define SETTINGS_DIVIDER_24             7

#define SETTINGS(x)         (EEPROM_GetSettings()->x)
#define CALIBRATION(x, y)   (EEPROM_GetCalibration()->offset[x][y])

#include "midi2cv.h"
#include "calibrate.h"

typedef struct
{
    uint8_t cv2_mode;
    uint8_t cv2_cc;
    uint8_t divider;
    bool_t exp_velocity;
    bool_t retrigger;
    bool_t regate;
    uint8_t stack_mode;
    uint8_t num_voices;
    bool_t velocity_unit;
    bool_t persistent_lock;
} settings_t;

typedef struct
{
    int8_t offset[CALIBRATE_NUM_CV_OUTS][CALIBRATE_LUT_SIZE];
} calibration_t;

void EEPROM_Init(void);
bool_t EEPROM_SaveSettings(void);
inline settings_t *EEPROM_GetSettings(void);
bool_t EEPROM_SaveCalibration(void);
inline calibration_t *EEPROM_GetCalibration(void);


#endif

