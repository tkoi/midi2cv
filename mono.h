#ifndef MONO_H_
#define MONO_H_

#include "midi2cv.h"
#include "voice.h"

void Mono_Init(Voice_t *voice);
void Mono_NoteOn(uint8_t note, uint8_t velocity);
void Mono_NoteOff(uint8_t note, uint8_t velocity);
void Mono_Pitchbend(uint8_t lsbits, uint8_t msbits);
void Mono_ChannelPressure(uint8_t pressure);
void Mono_ControlChange(uint8_t control, uint8_t value);
inline void Mono_Tick(void);


#endif

