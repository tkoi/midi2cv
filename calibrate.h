#ifndef CALIBRATE_H_
#define CALIBRATE_H_

#include "midi2cv.h"

#define CALIBRATE_NUM_CV_OUTS   4
#define CALIBRATE_LUT_SIZE      10

void Calibrate_Init(void);
void Calibrate_Begin(void);
bool_t Calibrate_NoteOn(uint8_t note, uint8_t velocity);
uint16_t Calibrate_GetCalibratedPitch(uint8_t dac, uint8_t note);


#endif

